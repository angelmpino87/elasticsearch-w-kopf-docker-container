FROM elasticsearch:2.3

MAINTAINER Angel Pino <apino@agentsbud.com>

RUN echo "Installing lmenezes/elasticsearch-kopf plugin"

RUN /usr/share/elasticsearch/bin/plugin install lmenezes/elasticsearch-kopf/2.0
